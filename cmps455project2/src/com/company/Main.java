package com.company;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Semaphore;
import java.util.concurrent.ThreadLocalRandom;
import static java.lang.System.exit;

class Shared {
    public static int domains;
    public static int objects;
    public static Semaphore[] locks;
    public static String[][] access_matrix;
    public static int getActionPerm(){
        return ThreadLocalRandom.current().nextInt(0, (domains + objects));
    }
    public static int getReadOrWrite(){
        return ThreadLocalRandom.current().nextInt(0, 2);
    }
    public static int getRandomYield(){
        return ThreadLocalRandom.current().nextInt(3, 8 );
    }
}

public class Main {
    public static int getRandomDimension(){ //creates a random number from 3-7 to act as the dimensions
        return ThreadLocalRandom.current().nextInt(3,8  );
    }
    public static int getRandomRWPerm(){ //creates a random number from 0-3 to act as the read/write permissions
        return ThreadLocalRandom.current().nextInt(0, 4 );
    }
    public static int getRandomSwitchPerm(){ //creates a random number from 0-1 to act as the switch permissions
        return ThreadLocalRandom.current().nextInt(0, 2 );
    }
    // Start code by Allen Lin
    public static void print_matrix(String[][] mat, int domain, int object){
        System.out.println("Domain count: " + domain);
        System.out.println("Object count: " + object);
        System.out.print("Domain/Object     ");
        for(int i = 0; i < object; i++){
            System.out.printf ("%-8s", "F" + (i+1));
        }
        for(int i = 0; i <domain; i++){
            System.out.printf ("%-8s", "D" + (i+1));
        }
        System.out.println();
        for(int i = 0; i < domain; i++){
            System.out.print("            " + "D" + (i+1) + "    ");
            for(int j = 0; j < domain + object; j++){
                System.out.printf ("%-8s", mat[i][j]);
            }
            System.out.println();
        }
    }

    public static int getRanRW(){
        // 0 = Nothing, 1 = Read, 2 = Write, 3 = Both
        return ThreadLocalRandom.current().nextInt(0,4);
    }
    public static int getRanSwi(){
        // 0 = False, 1 = True
        return ThreadLocalRandom.current().nextInt(0,2);
    }
    public static int ranDomAndObj(){
        return ThreadLocalRandom.current().nextInt(3,8);
    }

    public static void main(String[] args) {
        if (args.length == 1) {
            switch(args[0]) {
                case "M":
                    access_Matrix();
                    break;
                case "L":
                    list();
                    break;
                case "C":
                    cap();
                    break;
                default:
                    System.out.println(args[0] + " is not a valid input use M, L, or C");
                    exit(0);

            }
        }
        else{
            System.out.println("Not a valid input use M, L, or C");
            exit(0);
        }
    }

    public static void access_Matrix() {
        int num_domain = getRandomDimension();
        Shared.domains = num_domain;
        int num_objects = getRandomDimension();
        Shared.objects = num_objects;
        Shared.locks = new Semaphore[num_objects];
        for(int i = 0; i < num_objects;i++){
            Shared.locks[i] = new Semaphore(1);
        }

        Shared.access_matrix = new String[num_domain][num_domain + num_objects];
        for(int i = 0; i < num_domain; i++){ //initializes the values in the matrix to r/w or allow/disallow
            for(int j = 0; j < (num_domain + num_objects); j++){
                if(j >= num_objects){ // if j >= number of objects input allow or " " instead of R/W
                    int switch_perm = getRandomSwitchPerm();
                    if((j - num_objects) == i ) // set the mat[i][j]
                        Shared.access_matrix[i][j] = "-";
                    else if(switch_perm == 0)
                        Shared.access_matrix[i][j] = "";
                    else
                        Shared.access_matrix[i][j] = "allow";
                }
                else{ // if j < number of objects input R/W
                    switch(getRandomRWPerm()){
                        case 0:
                            Shared.access_matrix[i][j] = "";
                            break;
                        case 1:
                            Shared.access_matrix[i][j] = "R";
                            break;
                        case 2:
                            Shared.access_matrix[i][j] = "W";
                            break;
                        case 3:
                            Shared.access_matrix[i][j] = "R/W";
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        print_matrix(Shared.access_matrix, num_domain, num_objects); //prints out the access matrix look at the function to see how it works
        for(int i = 0; i < num_domain; i++){
            String thread_name = "Thread: " + i;
            matrixThread thread = new matrixThread(thread_name, i );
            thread.start();
        }
    }
    //End code by Allen Lin

    // Begin code changes by Tony Huynh
    public static void list(){
        Random rand = new Random();
        int num_domains = rand.nextInt(5)+3;
        int num_objects = rand.nextInt(5)+3;
        int rand_num;
        Semaphore[] free_space_sem = new Semaphore[num_objects+num_domains];
        for (int i=0; i< free_space_sem.length; i++) {
            free_space_sem[i] = new Semaphore(1);
        }
        List<String>[] access_list = new List[num_objects+num_domains];
        for (int i = 0; i<access_list.length; i++) {
            access_list[i] = new ArrayList<String>();
        }
        for (int i =0; i<num_objects; i++) {
            access_list[i].add("F" + i);
            access_list[i].add("-->");
            // Prevents empty perms
            while (access_list[i].size() == 2) {
                for (int k = 0; k < num_domains; k++) {
                    rand_num = rand.nextInt(2);
                    if (rand_num == 1) {
                        rand_num = rand.nextInt(4);
                        switch (rand_num) {
                            case 0:
                                // charAt(string.length-2)
                                access_list[i].add("D" + k + ":R");
                                break;
                            case 1:
                                access_list[i].add("D" + k + ":W");
                                break;
                            case 2:
                                access_list[i].add("D" + k + ":R/W");
                                break;
                            case 3:
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
        for (int i =0; i<num_domains; i++) {
            access_list[i+num_objects].add("D" + i);
            access_list[i+num_objects].add("-->");
            // Prevents empty perms
            while (access_list[i+num_objects].size() == 2) {
                for (int k = 0; k < num_domains; k++) {
                    rand_num = rand.nextInt(2);
                    if (rand_num == 1 && k != i) {
                        access_list[i + num_objects].add("D" + k + ":allow");
                    }
                }
            }
        }
        System.out.println("Domain Count: " + num_domains);
        System.out.println("Object Count: " + num_objects);
        for (int i=0; i< access_list.length; i++)
            System.out.println(access_list[i]);
        for (int i=0; i< num_domains; i++) {
            AccessList mikeneko = new AccessList(free_space_sem,i, 5, access_list, num_objects, num_domains);
            mikeneko.start();
        }
        // End code changes by Tony Huynh
    }
        // Start code changes by Paul Tran
    public static void cap(){
        int D = ranDomAndObj();
        int O = ranDomAndObj();
        while(true) {
            System.out.println("Access control scheme: Capability List");
            System.out.println("Domain Count: "+D);
            System.out.println("Object Count: "+O+"\n");

            if(D > 0 && O > 0){
                break;
            }
        }
        cap_list(D,O);
    }

    public static void cap_list(int D, int O){
        CLShare.dom = D;
        CLShare.obj = O;
        CLShare.capability_list = new int [D][(D+O)];
        CLShare.check = new Semaphore[O];
        for(int i = 0; i<O;i++){
            // To check if the File is available to read/write...
            CLShare.check[i] = new Semaphore(1);
        }

        for(int i = 0; i < D;i++){
            for(int j = 0; j<(D+O);j++){
                if(j < O){
                    int ran = getRanRW();
                    if(ran == 0){
                        CLShare.capability_list[i][j] = 0;
                    }
                    else if(ran == 1){
                        CLShare.capability_list[i][j] = 1;
                    }
                    else if(ran == 2){
                        CLShare.capability_list[i][j] = 2;
                    }
                    else{
                        CLShare.capability_list[i][j] = 3;
                    }
                }
                else{
                    int ran = getRanSwi();
                    if(i == (j - O)){
                        CLShare.capability_list[i][j] = 0;
                    }
                    else if(ran == 0){
                        CLShare.capability_list[i][j] = 0;
                    }
                    else{
                        CLShare.capability_list[i][j] = 1;
                    }
                }
            }
        }

        for(int i = 0;i<D;i++){
            System.out.print("D"+(i+1)+" -->");
            for(int j = 0;j<(D+O);j++){
                if(j<O){
                    if(CLShare.capability_list[i][j] == 1) {
                        System.out.print(" F" + (j + 1) + ": Read,");
                    }
                    else if(CLShare.capability_list[i][j] == 2){
                        System.out.print(" F" + (j + 1) + ": Write,");
                    }
                    else if(CLShare.capability_list[i][j] == 3){
                        System.out.print(" F"+(j + 1)+": Read/Write,");
                    }
                }
                else{
                    if(CLShare.capability_list[i][j] == 1){
                        System.out.print(" D"+(j-O+1)+": Allow,");
                    }
                }
            }
            System.out.println();
        }
        System.out.println();
        for(int i = 0; i<D;i++){
            String name = "Thread: "+i;
            CapabilityList cap = new CapabilityList(i,name);
            cap.start();
        }
    }
}

class CLShare {
    public static int dom;
    public static int obj;
    public static int[][] capability_list;
    public static Semaphore[] check;

    public static int RWorSwi() {
        // 0 = RW, 1 = Switch
        return ThreadLocalRandom.current().nextInt(0, 2);
    }

    public static int ranF(){
        return ThreadLocalRandom.current().nextInt(0,obj);
    }

    public static int ranRW(){
        // 0 = R, 1 = W
        return ThreadLocalRandom.current().nextInt(0,2);
    }

    public static int ranD(){
        return ThreadLocalRandom.current().nextInt(0,dom);
    }

    public static int Yield() {
        return ThreadLocalRandom.current().nextInt(3, 8);
    }
}
    // End code changes by Paul Tran
